"""
spacetraders_sdk | https://codeberg.org/dataCobra/spacetraders_sdk

Copyright (C) 2021, Benedikt Tuchen <benedikt.tuchen@thinkbot.de>

spacetraders_sdk is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

spacetraders_sdk is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import requests
from datetime import datetime
from typing import Union

apiurl: str = "https://api.spacetraders.io"


def check_error(request: dict) -> Union[str, None]:
    """Check for an error and return it when found.

    Parameters:
        request (dict): Request returned by the server

    Returns:
        str: error code and error message
    """

    if "error" in request.keys():
        errcode: str = request["error"]["code"]
        errmsg: str = request["error"]["message"]
        return f"Error {errcode}: {errmsg}"
    else:
        return None


def check_alive() -> bool:
    """Check whether the server is alive or not

    Returns:
        bool: True == available / False == unavailable
    """

    req: dict = requests.get(apiurl + "/game/status").json()
    error = check_error(req)
    if error is None:
        return True
    else:
        print(error)
        return False


def create_user(username: str) -> Union[str, None]:
    """Create a user to start playing with the API

    Parameters:
        username (str): Username for the new account

    Returns:
        str: token
    """

    req: dict = requests.post(apiurl + "/users/" + username + "/claim").json()
    error = check_error(req)
    if error is None:
        token: str = req["token"]
        return token
    else:
        print(error)
        return None


def get_user_info(token: str) -> Union[tuple, None]:
    """Get the relevant user information. The token is necessary for the query.

    Parameters:
        token (str): Token of the username

    Returns:
        tuple: credits (int),
               shipCount (int),
               structureCount (int),
               joinedAt (datetime)
    """

    req: dict = requests.get(apiurl + "/my/account",
                             params={"token": token}).json()
    error = check_error(req)
    if error is None:
        credits: int = req["user"]["credits"]
        shipCount: int = req["user"]["shipCount"]
        structureCount: int = req["user"]["structureCount"]
        joinedAt: datetime = datetime.strptime(req["user"]["joinedAt"],
                                               "%Y-%m-%dT%H:%M:%S.%fZ")
        return credits, shipCount, structureCount, joinedAt
    else:
        print(error)
        return None
