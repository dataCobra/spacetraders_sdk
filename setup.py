import setuptools

with open('requirements.txt', 'r') as f:
    install_requires = f.read().splitlines()

setuptools.setup(
    name='spacetraders_sdk',
    packages=['spacetraders_sdk'],
    install_requires=install_requires
)
